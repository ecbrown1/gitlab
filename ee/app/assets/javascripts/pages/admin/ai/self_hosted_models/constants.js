export const SELF_HOSTED_MODEL_MUTATIONS = {
  CREATE: 'aiSelfHostedModelCreate',
  UPDATE: 'aiSelfHostedModelUpdate',
};
